var gulp = require('gulp'),
  clean = require('gulp-clean'),
  watch = require('gulp-watch'),
  plumber = require('gulp-plumber'),
  sass = require('gulp-sass'),
  sourcemaps = require('gulp-sourcemaps'),
  // csscomb = require('gulp-csscomb'),
  autoprefixer = require('gulp-autoprefixer'),
  connect = require('gulp-connect'),
  ejslocals = require('gulp-ejs-locals'),
  imagemin = require('gulp-imagemin'),
  pngquant = require('imagemin-pngquant'),
  prettify = require('gulp-prettify')

var fs = require('fs');

// Path
var src = './public/src';
var dist = './public/dist';

// Server
gulp.task('serve', function() {
  connect.server({
    root: dist,
    port: 8001,
    livereload: true,
    open: {
      browser: 'chrome'
    }
  });
});

//sass -> css
gulp.task('sass', function () {
  return gulp.src(src+'/scss/**/*.scss')
  .pipe( plumber({
      errorHandler: function (error) {
        console.log(error.message);
        this.emit('end');
      }
    }) )
  .pipe(sourcemaps.init())
  .pipe(sass({'outputStyle':'expanded'}).on('error', sass.logError))
  .pipe(autoprefixer({
    browsers: ['last 3 versions','IE 8','android 2.3'],
    cascade: false
  }))
  // .pipe(csscomb())
  .pipe(sourcemaps.write('./maps'))
  .pipe(gulp.dest(dist+'/css'))
  .pipe(connect.reload())
});

//css
gulp.task('css', function () {
  return gulp.src([src+'/css/*.css',  '!' + src+'/css/libs/*.css'])
  .pipe( plumber({
      errorHandler: function (error) {
        console.log(error.message);
        this.emit('end');
      }
    }) )
  .pipe(gulp.dest(dist+'/css'))
  .pipe(connect.reload())
});
// ejs -> HTML
gulp.task('ejs', function() {
  gulp
    .src([src+'/ejs/**/*.ejs',  '!' + src+'/ejs/**/_*.ejs'])
    
    .pipe(ejslocals(
      {jsonData: JSON.parse(fs.readFileSync(src+'/ejs/index.json'))},
      {ext: '.html'}
    ))
    .pipe(prettify({indent_size: 1, indent_char:'\t'}))
    .pipe( plumber({
        errorHandler: function (error) {
          console.log(error.message);
          this.emit('end');
        }
      }) )
    .pipe(gulp.dest(dist+'/html'))
    .pipe(connect.reload())
});

// ejs_en -> HTML_en
gulp.task('ejs_en', function() {
  gulp
    .src([src+'/ejs_en/**/*.ejs',  '!' + src+'/ejs_en/**/_*.ejs'])
    
    .pipe(ejslocals(
      {jsonData: JSON.parse(fs.readFileSync(src+'/ejs_en/index.json'))},
      {ext: '.html'}
    ))
    .pipe(prettify({indent_size: 1, indent_char:'\t'}))
    .pipe( plumber({
        errorHandler: function (error) {
          console.log(error.message);
          this.emit('end');
        }
      }) )
    .pipe(gulp.dest(dist+'/html_en'))
    .pipe(connect.reload())
});

// ejs_zh -> HTML_zh
gulp.task('ejs_zh', function() {
  gulp
    .src([src+'/ejs_zh/**/*.ejs',  '!' + src+'/ejs_zh/**/_*.ejs'])
    
    .pipe(ejslocals(
      {jsonData: JSON.parse(fs.readFileSync(src+'/ejs_zh/index.json'))},
      {ext: '.html'}
    ))
    .pipe(prettify({indent_size: 1, indent_char:'\t'}))
    .pipe( plumber({
        errorHandler: function (error) {
          console.log(error.message);
          this.emit('end');
        }
      }) )
    .pipe(gulp.dest(dist+'/html_zh'))
    .pipe(connect.reload())
});

// ejs_ja -> HTML_ja
gulp.task('ejs_ja', function() {
  gulp
    .src([src+'/ejs_ja/**/*.ejs',  '!' + src+'/ejs_ja/**/_*.ejs'])
    
    .pipe(ejslocals(
      {jsonData: JSON.parse(fs.readFileSync(src+'/ejs_ja/index.json'))},
      {ext: '.html'}
    ))
    .pipe(prettify({indent_size: 1, indent_char:'\t'}))
    .pipe( plumber({
        errorHandler: function (error) {
          console.log(error.message);
          this.emit('end');
        }
      }) )
    .pipe(gulp.dest(dist+'/html_ja'))
    .pipe(connect.reload())
});

// js hint
gulp.task('js:hint', function () {
  gulp
    .src([src+'/js/*.js',  '!' + src+'/js/libs/*.js'])
    .pipe(gulp.dest(dist+'/js'))
    .pipe(connect.reload())
});

// image
gulp.task('imgmin', function() {
  gulp
    .src([src+'/images/**/*.{png,jpg,gif}'])
    .pipe(imagemin({
      progressive: true,
        interlaced:true,
      use: [pngquant()]
    }))
    .pipe(gulp.dest(dist+'/images'))
    .pipe(connect.reload())
});

// font
gulp.task('font', function () {
  gulp
    .src([src+'/fonts/**/*'])
    .pipe(gulp.dest(dist+'/fonts'))
});

// library
gulp.task('libs', function () {
  gulp
    .src([src+'/libs/**/*', '!' + src+'/libs/**/scss/**/*'])
    .pipe(gulp.dest(dist+'/libs'))
    .pipe(connect.reload())
});

//bootstrap
gulp.task('bootstrap', function () {
  return gulp.src(src+'/libs/bootstrap/scss/**/*.scss')
  .pipe( plumber({
      errorHandler: function (error) {
        console.log(error.message);
        this.emit('end');
      }
    }) )
  .pipe(sass())
  .pipe(gulp.dest(dist+'/libs/bootstrap/css'))
  .pipe(connect.reload())
});

// index
gulp.task('index', function () {
  gulp
    .src([src+'/*.html'])
    .pipe(gulp.dest(dist))
});

//clean 작업 설정

gulp.task('clean', function(){
  return gulp.src([dist+'/*',dist+'/*.html','!' + dist+'/.git', '!' + dist +'/*.md', '!' + dist +'/.gitignore'], {read: false})
    .pipe(clean());
});


// Watch task
gulp.task('watch',[], function() {
  watch(src+'/ejs/**/*', function() {
    gulp.start('ejs');
  });
  watch(src+'/ejs_en/**/*', function() {
    gulp.start('ejs_en');
  });
  watch(src+'/ejs_zh/**/*', function() {
    gulp.start('ejs_zh');
  });
  watch(src+'/ejs_ja/**/*', function() {
    gulp.start('ejs_ja');
  });
  watch(src+'/scss/**/*.{scss,sass}', function() {
    gulp.start(['sass']);
  });
  watch([src+'/css/*.css',  '!' + src+'/css/libs/*.css'], function() {
    gulp.start(['css']);
  });
  watch([src+'/js/*.js',  '!' + src+'/js/libs/*.js'], function() {
    gulp.start('js:hint');
  });
  watch(src+'/images/**/*.{png,jpg,gif}', function() {
    gulp.start('imgmin');
  });
  watch([src+'/index.html', src+'/list.html'], function() {
    gulp.start('index');
  });
  watch([src+'/libs/**/*'], function() {
    gulp.start('libs');
  });
  watch([src+'/libs/bootstrap/scss/**/*.scss'], function() {
    gulp.start('bootstrap');
  });
});

gulp.task('default', ['serve','sass','css','ejs','ejs_en','ejs_zh','ejs_ja','js:hint','imgmin','watch','font','index','libs','bootstrap']);
