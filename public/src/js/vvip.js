$(function() {
  var applicationBtn = $("[data-scroll='true']");
  var applicationPosition;
  var headerSize;
  $(applicationBtn).on('click', function (event) {
    event.preventDefault();
    var btnAttr = $(this).attr('href');
    applicationPosition = $(btnAttr).offset().top;
    $("body").hasClass("media-mobile") ? headerSize = 45 : headerSize = 70;
    $("html, body").animate({ scrollTop: applicationPosition - headerSize }, 500);
  });
});