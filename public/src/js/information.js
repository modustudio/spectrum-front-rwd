window.addEventListener("DOMContentLoaded", function() {
  if(document.getElementById('map') !== null){
    initMap();
    setApi();
  }
});

$(function() {
  $(".info-board__list .js-toggle, .info-board2__list .js-toggle").on("click keypress", function(e) {
    if (e.keyCode == 13 || e.type == "click")
      $(this).parent("li").toggleClass("is-open").siblings().removeClass("is-open");
  });
});

$(function() {
  var tab = $(".information-tab");
  var index = 0;
  var hash = window.location.hash;
  var tt= [];

  var dimmed = '<div class="dropdown-dimmed js-dimmed"></div>'
  var $dimmed = ".js-dimmed";

  $(tab).find("a").each(function(idx){
    var t = $(tab).find("a").eq(idx).attr("href").split('#')[1];
    $(this).on("click", function(e){
      if (this.href.match(/#([^ ]*)/g)) {
        e.preventDefault();
        sele($(this));
        $("#"+t).addClass("is-active").siblings().removeClass('is-active').parent().removeClass('is-all');
        if(t==='none'){
          $(".stage-map__section").removeClass('is-active').parent().addClass('is-all');
        }
      }
    });
  });

  $(tab).find("[class$=list]").on("click", function(){
    if ($("body").hasClass("media-mobile")) {
      if ($(this).hasClass("js-open-m")) {
        $(this).removeClass("js-open-m");
        $(this).closest($(tab)).find($dimmed).remove();
      } else {
        $(this).addClass("js-open-m");
        $(this).closest($(tab)).append(dimmed);
      }
    }
  });

  if ($("body").hasClass("media-mobile") && tab.length > 0) {
    $(document).on("click touchend",function(e){
      if (!$(e.target).closest('div').hasClass('information-tab') === true && $(e.target).closest($(tab)).hasClass("js-open-m") == false) {
        $(tab).children().eq(0).removeClass("js-open-m");
        $($dimmed).remove();
      }
    });
  }

  function sele(el) {
    $(el).parent().addClass("is-active").siblings().removeClass("is-active");
  }

  $(window).resize(function(){
    if ($('body').attr('data-mobile') == 'false') $(".tab").removeClass("js-open-m");
  });
});