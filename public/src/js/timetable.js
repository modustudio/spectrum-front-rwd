$(function() {
  var $dropDown = $("[data-drop-down]");
  var $state = $dropDown.find(".js-state");
  var $list = $dropDown.find(".js-list");
  var $item = $dropDown.find(".js-item");
  var toggoleOpen = "is-open";
  var on = "js-active";
  var $timeList = $('.time-table__list');

  $item.on("click",function(e){
    // if (this.tagName != "A") e.preventDefault();
    $(this).closest($list).siblings($state).find('span').text($(this).text());
    $(this).addClass(on);
    $(this).siblings().removeClass(on);
    $(this).closest($list).removeClass(toggoleOpen).siblings($state).removeClass(toggoleOpen);

    var category = $(this).find('a').attr('data-category');
    if(category !== 'is-all'){
      $timeList.removeClass('is-all').addClass('is-full');
      $timeList.find('.'+category).addClass('is-show').siblings().removeClass('is-show');
    } else if(category === 'is-all'){
      $timeList.removeClass('is-full').addClass('is-all');
      $timeList.children().removeClass('is-show');
    }
  });
});