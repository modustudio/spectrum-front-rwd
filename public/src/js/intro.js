$(window).on("load resize", function () {
  if (navigator.userAgent.match(/Android|Mobile|iP(hone|od|ad)|BlackBerry|IEMobile|Kindle|NetFront|Silk-Accelerated|(hpw|web)OS|Fennec|Minimo|Opera M(obi|ini)|Blazer|Dolfin|Dolphin|Skyfire|Zune/)) {
    $('body').addClass('device-mobile')
  } else {
    $('body').removeClass('device-mobile')
  }
  if ($(document).width() <= 1024) {
    $('body').addClass('media-mobile');
  } else {
    $('body').removeClass('media-mobile');
  }
}).resize();

function introVideo(el) {
  $('.js-intro-video').show();
  var player = document.getElementById('player');
  if (!$('body').hasClass('device-mobile')) fullscreen(player)
}
// $(function(){
//   var player = document.getElementById('player');
//   $('.js-video-close').on('click',function(){
//     player.contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
//     $('.js-intro-video').hide();    
//   })
// })

function fullscreen(el) {
  if (~el.src.indexOf('?')) el.src += '&autoplay=1';
  else el.src += '?autoplay=1';

  var req = el.requestFullscreen
      || el.webkitRequestFullscreen
      || el.mozRequestFullScreen
      || el.msRequestFullscreen;

  req.call(el);
}