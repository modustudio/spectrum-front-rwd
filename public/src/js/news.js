$(function() {
  $(".news-board__list .js-toggle").on("click keypress", function(e) {
    if (e.keyCode == 13 || e.type == "click")
      $(this).parent("li").toggleClass("is-open").siblings().removeClass("is-open");
      $(window).trigger('resize');
  });
});
