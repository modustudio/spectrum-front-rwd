$(function(){
  setTimeout(function(){
    $('.wrapper').animate({'opacity':'1'});
    // $('.js-main-visual-content').addClass('is-view');
  },50);
})

$(window).on("load resize", function () {
  if (navigator.userAgent.match(/Android|Mobile|iP(hone|od|ad)|BlackBerry|IEMobile|Kindle|NetFront|Silk-Accelerated|(hpw|web)OS|Fennec|Minimo|Opera M(obi|ini)|Blazer|Dolfin|Dolphin|Skyfire|Zune/)) {
    $('body').addClass('device-mobile')
  } else {
    $('body').removeClass('device-mobile')
  }
  if ($(document).width() <= 1024) {
    $('body').addClass('media-mobile');
  } else {
    $('body').removeClass('media-mobile');
  }
}).resize();


// GNB
$(function () {
  var gnbItem = '.site-gnb__item';
  var gnbLink = '.site-gnb__link';

  $(gnbItem).on('mouseenter', function () {
    if ($('body').hasClass('media-mobile') || $('body').hasClass('device-mobile')) return;
    $(this).addClass('is-over');
  });
  $(gnbItem).on('mouseleave', function () {
    if ($('body').hasClass('media-mobile') || $('body').hasClass('device-mobile')) return;
    $(this).removeClass('is-over')
  });
  $(gnbItem).find('a').on('focusin', function () {
    if ($('body').hasClass('media-mobile') || $('body').hasClass('device-mobile')) return;
    $(this).closest(gnbItem).trigger('mouseenter');
  });
  $(gnbItem).find('a:last').on('focusout', function () {
    if ($('body').hasClass('media-mobile') || $('body').hasClass('device-mobile')) return;
    $(this).closest(gnbItem).trigger('mouseleave');
  });
  $(gnbLink).on('click', function(e) {
    if ($(this).closest(gnbItem).find('ul').length == 0) return; 
    e.preventDefault();
    if ($(this).closest(gnbItem).hasClass('is-over')) {
      $(this).closest(gnbItem).removeClass('is-over');
    } else {
      $(this).closest(gnbItem).addClass('is-over');
    }
  });
  $(window).resize(function () {
    if (!$('body').hasClass('media-mobile')) $(gnbItem).removeClass('is-over');
  });
})

// header fixed
$(function() {
  $(window).on("scroll resize", function (e) {
    if (!$('body').hasClass('media-mobile')) {
      headerFixed('.site-gnb');
      $('.mobile-header').removeClass('is-fixed');
    } else {
      headerFixed('.mobile-header');
      $('.site-gnb').removeClass('is-fixed');
    }
  });
  function headerFixed(target) {
    if (!$(target).attr('data-top')) $(target).attr('data-top', $(target)[0].offsetTop);
    if ($(this).scrollTop() > $(target).attr('data-top')) {
      $(target).addClass('is-fixed');
    } else {
      $(target).removeClass('is-fixed');
    }
  }
})

// mobile menu
$(function () {
  $(document).on('click','.mobile-button-open, .mobile-button-close',function () {
    if ($('.mobile-menu').hasClass('is-on')) {
      $('.mobile-menu').removeClass('is-on')
      // $('body').removeClass('is-not-scroll')
      $('.mobile-menu-dimmed').remove();
    } else {
      $('.mobile-menu').addClass('is-on')
      // $('body').addClass('is-not-scroll')
      $('<div class="mobile-menu-dimmed"></div>').appendTo($(document.body))
    }
  })
  $(window).resize(function () {
    if (!$('body').hasClass('media-mobile')) {
      $('.mobile-menu').removeClass('is-on'); 
      // $('body').removeClass('is-not-scroll');
      $('.mobile-menu-dimmed').remove();
      $('.mobile-button-open').attr('disabled','disabled');
      $('.mobile-button-close').attr('disabled','disabled');
    } else { 
      $('.mobile-button-open').removeAttr('disabled');
      $('.mobile-button-close').removeAttr('disabled');
    }
  });
})


// 다국어
$(function() {
  var langBtn = '.site-language__location'
  var langList = '.site-language__list'
  $(langBtn).attr('tabindex','0').css({ 'cursor' : 'pointer' });
  $(document).on('click', langBtn,function(){
    if (!$(langList).hasClass('is-on')) {
      $(langList).addClass('is-on')
    } else {
      $(langList).removeClass('is-on')
    }
  })
  $(document).on('click', function(e) {
    if ($(langList).hasClass('is-on') && $(e.target).closest(langList).hasClass('is-on') == false && $(e.target).closest(langBtn).length == 0) {
      $(langList).removeClass('is-on');
    }
  });
  $(window).on('scroll resize', function(e) {
    $(langList).removeClass('is-on');
  });
})

// dropDown
$(function(){
  var $dropDown = $("[data-drop-down]");
  var $state = $dropDown.find(".js-state");
  var $list = $dropDown.find(".js-list");
  var toggoleOpen = "is-open";
  var dimmed = '<div class="dropdown-dimmed js-dimmed"></div>'
  var $dimmed = ".js-dimmed";

  $state.on("click keypress",function(e){
    if ((e.keyCode == 13)||(e.type == "click")) {
      if ($(this).hasClass(toggoleOpen)) {
        $(this).closest($dropDown).removeClass(toggoleOpen);
        $(this).removeClass(toggoleOpen).siblings($list).removeClass(toggoleOpen);
        if ($("body").hasClass("media-mobile")) {
          $(this).closest($dropDown).find($dimmed).remove();
        }
      } else {
        $(this).closest($dropDown).addClass(toggoleOpen);
        $(this).addClass(toggoleOpen).siblings($list).addClass(toggoleOpen);
        if ($("body").hasClass("media-mobile")) {
          $(this).closest($dropDown).append(dimmed);
        }
      }
    }
  });
  $(document).on("click touchend",function(e){
    if (!$(e.target).hasClass("js-state") && $list.hasClass(toggoleOpen) && $(e.target).closest($list).hasClass(toggoleOpen) == false) {
      $list.removeClass(toggoleOpen);
      $state.removeClass(toggoleOpen);
      $dropDown.removeClass(toggoleOpen);
      $($dimmed).remove();
    }
  });
});

//tab
$(function() {
  var tab = $("[data-tab='true']");
  var index = 0;
  var hash = window.location.hash;
  var tt= [];
  var dimmed = '<div class="dropdown-dimmed js-dimmed"></div>'
  var $dimmed = ".js-dimmed";

  $(tab).find("a").each(function(idx){
    var t = $(tab).find("a").eq(idx).attr("href").split('#')[1];
    tt.push(t);
    if (hash) {
      index = /\d+/.exec(hash)[0];
      index = (parseInt(index) || 1) ;
      sele($(tab).find("a[href="+hash+"]"));
      for (var i in tt) {
        $("#"+tt[i]).removeClass("is-active");
      }
      $(hash).addClass("is-active");
    }

    $(this).on("click", function(e){
      if (this.href.match(/#([^ ]*)/g)) {
        e.preventDefault();
        if (!$(this).parent().hasClass("is-active")) window.location.hash = /\d+/.exec($(this).attr("href")).input;
        sele($(this));
        for (var i in tt) {
          $("#"+tt[i]).removeClass("is-active");
        }
        $("#"+t).addClass("is-active");
      }
    });
  });

  $(tab).find("[class$=list]").on("click", function(){
    if ($("body").hasClass("media-mobile")) {
      if ($(this).hasClass("js-open-m")) {
        $(this).removeClass("js-open-m");
        $(this).closest($(tab)).find($dimmed).remove();
      } else {
        $(this).addClass("js-open-m");
        $(this).closest($(tab)).append(dimmed);
      }
    }
  });

  if ($("body").hasClass("media-mobile") && tab.length > 0) {
    $(document).on("click touchend",function(e){
      if (!$(e.target).closest('div').attr('data-tab') === true && $(e.target).closest($(tab)).hasClass("js-open-m") == false) {
        $(tab).children().eq(0).removeClass("js-open-m");
        $($dimmed).remove();
      }
    });
  }

  function sele(el) {
    $(el).parent().addClass("is-active").siblings().removeClass("is-active");
  }

  $(window).resize(function(){
    if ($('body').attr('data-mobile') == 'false') $(".tab").removeClass("js-open-m");
  });
});

//layer popup
$(function () {
  var $popupBtn = $("[data-popup*=popup]");
  var $popupWrap = $(".layer-popup2");

  $popupBtn.on('click', function (event) {
    event.preventDefault();
    var $this = $(this);
    var element;
    if($this.attr('href')){
      element = $this.attr('href');
    }
    if($this.attr('data-url')){
      var videoID = youtube_parser($this.attr('data-url'));
    }
    layerPopup(element,videoID);
  });
  $popupWrap.on('click', function (e) {
    if(!$(e.target).hasClass('is-popup')){
      $("body").removeAttr("has-dimmed");
      $('body').removeClass('is-not-scroll');
      $(".dimmed").fadeOut();
      $(this).fadeOut();

      if($(this).find('.is-iframe').length > 0){
        $(this).find('.is-iframe').empty();
      }
    }
  });
  function layerPopup(el,val) {
    var isDim = $("body").attr("has-dimmed");
    if ($(el).length === 0) {
      alert("레이어팝업 없음");
      return false;
    }
    if(!isDim){
      $("body").attr("has-dimmed",true);
      $('body').addClass('is-not-scroll');
      $(".dimmed").fadeIn();
      $(el).fadeIn();

      if($(el).find('.is-iframe').length > 0){
        var $iframe = '<iframe class="iframe" width="100%" height="100%" src="https://www.youtube.com/embed/'+val+'?rel=0" frameborder="0" allow="encrypted-media" allowfullscreen></iframe>'
        $(el).find('.is-iframe').empty();
        $(el).find('.is-iframe').prepend($iframe);
      }
    } else {
      $("body").removeAttr("has-dimmed");
      $('body').removeClass('is-not-scroll');
      $(".dimmed").fadeOut();
      $(el).fadeOut();

      if($(el).find('.is-iframe').length > 0){
        $(el).find('.is-iframe').empty();
      }
    }
  }
});

//dotdotdot
$(function () {
  $(window).on("resize", function() {
    var $target = $(".js-ellipsis");
    if($target.length > 0){
      $target.each(function () {
        $(this).dotdotdot({
          height: "watch",
          watch: "window"
        })
      });
    }
  }).trigger("resize"); 
});

//lazy img 
$(function(){
  $('.js-lazy-img').each(function(){
    var $this = $(this);
    if ($this.offset().top < $(window).scrollTop() + $(window).height() && $this.offset().top > $(window).scrollTop() - $this.height()) {
      var src = $this.attr('data-src');
      $this.attr('src', src).css({'opacity':'0'}).animate({'opacity':'1'},300);
    } else {
      var n = function() {
        if ($this.offset().top < $(window).scrollTop() + $(window).height() && $this.offset().top > $(window).scrollTop() - $this.height()) {
          var src = $this.attr('data-src');
          $this.attr('src', src).css({'opacity':'0'}).animate({'opacity':'1'},300);
          $(window).unbind('scroll', n);
        }
      };
      $(window).bind('scroll', n);
    }
  });
});

// youtube img
$(function(){
  $('.js-youtube-img').each(function(){
    var $this = $(this);
    if ($this.offset().top < $(window).scrollTop() + $(window).height() && $this.offset().top > $(window).scrollTop() - $this.height()) {
      var url = $this.closest('[data-url]').attr('data-url')
      var src = youtube_parser(url);
      $this.attr('src', 'http://img.youtube.com/vi/'+src+'/maxresdefault.jpg').css({'opacity':'0'}).animate({'opacity':'1'},300);
    } else {
      var n = function() {
        if ($this.offset().top < $(window).scrollTop() + $(window).height() && $this.offset().top > $(window).scrollTop() - $this.height()) {
          var src = youtube_parser($this.closest('[data-url]').attr('data-url'));
          $this.attr('src', 'http://img.youtube.com/vi/'+src+'/maxresdefault.jpg').css({'opacity':'0'}).animate({'opacity':'1'},300);
          $(window).unbind('scroll', n);
        }
      };
      $(window).bind('scroll', n);
    }
  });
});

// youtube title
$(function(){
  if ($('.js-youtube-tit').length > 0) {
    $('.js-youtube-tit').each(function(){
      var $this = $(this);
      var url = $this.closest('[data-url]').attr('data-url')
      $.getJSON('https://noembed.com/embed',
      {format: 'json', url: url}).done(function(data){
        $this.text(data.title)
      });
    });
  }
});

// youtube url get
function youtube_parser(url){
  var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
  var match = url.match(regExp);
  return (match&&match[7].length==11)? match[7] : false;
}

// 맨위로
$(function(){
  var gotopbtn = $('.js-go-to-top');
  var footHeight = $('.site-footer').height();
  var scrollPos;
  var docHeight;
  $(window).on("load resize", function () {
    if (!$('body').hasClass('media-mobile')) {
      docHeight = $(document).height() - $(window).height() - (footHeight);
      scrollPos = $(this).scrollTop();
      if (scrollPos > docHeight) gotopbtn.addClass("go-to-top--fixed");
      if ($(this).scrollTop() == 0) gotopbtn.hide().removeClass("go-to-top--fixed");
    } else { 
      gotopbtn.hide()
    }
  });
  $(window).scroll(function() {
    scrollPos = $(this).scrollTop();
    if (!$('body').hasClass('media-mobile')) {
      if (scrollPos > docHeight) {
        gotopbtn.addClass("go-to-top--fixed");
      } else  {
        gotopbtn.removeClass("go-to-top--fixed");
      }
      if (scrollPos == 0) {
        gotopbtn.fadeOut(300);
      } else {
        gotopbtn.fadeIn(300);
      }
    }
  });
	$(gotopbtn).on('click', function(e) {
		e.preventDefault();
		$('html, body').animate({
			scrollTop: 0
		}, 300);
	});
});

// google api 비동기 호출
var apiBool = false;
function setApi() {
  if(!apiBool){
    apiBool = true;
    var s = document.createElement('script');
    s.type = 'text/javascript';
    s.async = true;
    s.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBznY6blmbDvMqE7X_xhrdSu2ldeH-Vq3I&callback=initMap';
    var x = document.getElementsByTagName('script')[0];
    x.parentNode.insertBefore(s, x);

    return;
  }
}
var map;
function initMap() {
  if(apiBool){
    var haightAshbury = {lat: 37.515079, lng: 127.074782}; //37.515079, 127.074782
    map = new google.maps.Map(document.getElementById('map'), {
      zoom: 15,
      center: haightAshbury,
      mapTypeId: 'roadmap'
    });
    var marker = new google.maps.Marker({
      position: haightAshbury,
      map: map
    });
  }
}
