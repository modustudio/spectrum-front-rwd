$(function() {
  var ticketMainEl = ".ticket-main-info__box";
  var tickettxtEl = '.ticket-main-info__txt-wrap';
  var ticketScrollEl = '.ticket-main-info__scroll-box';
  var ticketTxtHeight = 0;
  var ticketListHeight = 0;  
  $(window).on("resize", function() {
    if ($("body").hasClass("media-mobile")){
      $(ticketMainEl).removeClass("animate-in");
      $(ticketScrollEl).css("height", "auto");
      return;
    } else {
      if (!$(ticketMainEl).hasClass('animate-in')){        
        ticketTxtHeight = $(tickettxtEl).outerHeight();
        ticketListHeight = 580 - ticketTxtHeight;
        $(ticketMainEl).each(function (index) {
          (function(that, i) {
            var t = setTimeout(function() {
              $(that).addClass("animate-in");
            }, 250 * i);
          })(this, index);
        });
        $(ticketScrollEl).scrollbar(); // scrollbar js
        $(ticketScrollEl).css("height", ticketListHeight + "px");
      }
    }
  }).trigger('resize');
});