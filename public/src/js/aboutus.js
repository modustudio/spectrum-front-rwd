$(function() {
  var $historySlider = $('.history__slider');
  if( $historySlider.find('.history__slide-item:not(.bx-clone)').length > 1 ){
    $historySlider.bxSlider({
      pager: false,
      infiniteLoop: false,
      hideControlOnEnd: true
    });
  }
});