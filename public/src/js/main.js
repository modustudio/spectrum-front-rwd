$(function() {
  $(window).on('resize', function () {
    $('.main-visual').height($(window).height())
  }).trigger('resize');
  
  setTimeout(function(){
    $('.js-main-visual-content').fadeIn();
    // $('.js-main-visual-content').addClass('is-view');
  },500);

  // $(window).on('mousewheel', function (e) {
  //   if (e.originalEvent.wheelDelta / 120 > 0) {
  //     // scrolling up
  //     if ($(document).scrollTop() <= $(window).height()) {
  //       goToTop()
  //       return false;
  //     }
  //   } else {
  //     // scrolling down
  //     if ($(document).scrollTop() == 0) {
  //       mainVisualDown()
  //       return false;
  //     }
  //   }
  // })
  function mainVisualDown() {
    $('html, body').stop().animate({ scrollTop: 0 + $(window).height() }, 500);
  }
  function goToTop() {
    $('html, body').stop().animate({ scrollTop: 0 }, 500);
  }
});

$(function() {
  var $mainVisualSlider = $('.js-main-visual-slider');
  if( $mainVisualSlider.find('.js-main-visual-item:not(.bx-clone)').length > 1 ){
    $mainVisualSlider.bxSlider({
      pager: false,
      infiniteLoop: true,
      hideControlOnEnd: true,
      touchEnabled:true,
      swipeThreshold: 100
    });
  }
});

function mainVisualVideo(el) {
  var url = youtube_parser($(el).data('url'));
  var youtube = '<iframe width="100%" height="100%" src="https://www.youtube.com/embed/'+url+'" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
  var dimmed = 'main-visual-dimmed';
  var video = 'main-visual-video';
  var close = '<button type="button" class="main-visual-video-close">close</button>';
  $('<div class="'+dimmed+'"></div>').appendTo('body').css({'opacity':'0'}).animate({'opacity':'1'},200,function(){ $('.'+video).animate({'opacity':'1'},500);});
  $('<div class="'+video+'">'+youtube+'</div>').appendTo('.'+dimmed).css({'opacity':'0'})
  $(close).appendTo('.'+video).on('click',function(){ $('.'+dimmed).remove()});
}